package utils;

import com.sun.javafx.geom.Edge;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public enum Browsers {
    FIREFOX {
        public WebDriver create() {
            System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
            return new FirefoxDriver();
        }
    }, CHROME {
        public WebDriver create() {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
            return new ChromeDriver();
        }
    }, EDGE {
        public WebDriver create() {
            System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/msedgedriver.exe");
            return new EdgeDriver();
        }
    }, IE11 {
        public WebDriver create() {
            System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
            return new InternetExplorerDriver();
        }
    };
    public WebDriver create(){
        return null;
    }
}