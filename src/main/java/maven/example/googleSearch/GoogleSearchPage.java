package maven.example.googleSearch;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleSearchPage extends BasePage {
    public static final String GOOGLE_LINK = "https://www.google.com";

    public GoogleSearchPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".a4bIc > input[name = 'q']")
    private WebElement searchField;

    public GoogleSearchPage openGoogleSearch() {
        driver.get(GOOGLE_LINK);
        return this;
    }

    public GoogleResultPage search(String text) {
        searchField.sendKeys(text);
        searchField.submit();
        return new GoogleResultPage(driver);
    }


}
