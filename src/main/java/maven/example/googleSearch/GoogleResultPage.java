package maven.example.googleSearch;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Waiters;

import java.util.List;

import static utils.Waiters.waitForPageLoading;

public class GoogleResultPage extends BasePage {
    public static final String AUTOMATIONPRACTISE = "http://automationpractice.com/index.php";

    public GoogleResultPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#search a[href*='automationpractice.com']")
    private List<WebElement> siteLinkList;

    @FindBy(css = "#search a[href*='automationpractice.com']")
    private WebElement siteLink;

    public void OpenUrlByName() {
        siteLinkList.get(0).click();
        waitForPageLoading(AUTOMATIONPRACTISE, driver, 5);
    }
}
