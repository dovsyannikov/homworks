package maven;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import utils.Browsers;

import java.util.concurrent.TimeUnit;


import static utils.Waiters.TIME_THIRD;
import static utils.Waiters.implicitWait;

public class BaseTest {
    private static WebDriver driver;

    @BeforeClass
    public static void InitializeBrowser() {
        driver = Browsers.CHROME.create();
//        driver = new ChromeDriver();
        driver.manage().window().maximize();
        implicitWait(driver, TIME_THIRD, TimeUnit.SECONDS);
        //driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
