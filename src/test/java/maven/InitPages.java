package maven;

import maven.example.googleSearch.GoogleResultPage;
import maven.example.googleSearch.GoogleSearchPage;
import org.testng.annotations.BeforeMethod;

import static maven.BaseTest.getDriver;

public interface InitPages {
    GoogleSearchPage googleSearchPage = new GoogleSearchPage(getDriver());
    GoogleResultPage googleResultPage = new GoogleResultPage(getDriver());
}
