package maven.tests;

import maven.BaseTest;
import maven.InitPages;
import maven.example.googleSearch.GoogleResultPage;
import maven.example.googleSearch.GoogleSearchPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class SearchTests extends BaseTest implements InitPages {

    @Test
    public void myFirstTest() {
        //GIVEN
        googleSearchPage.openGoogleSearch();
        //WHEN
        googleSearchPage.search("automationpractise");
        //AND
        googleResultPage.OpenUrlByName();
        Assert.assertEquals(getDriver().getCurrentUrl(), "http://automationpractice.com/index.php", getDriver().getCurrentUrl());
    }
}
